package com.aman;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aman.dao.RequestDao;
import com.aman.modelclass.Request;

@WebServlet("/contactus")
public class ContactUsServlet extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("contactus.jsp");
		requestDispatcher.forward(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Request req = new Request();
		RequestDao requestDao = new RequestDao();
		String statusMessage = new String();
		req.setName(request.getParameter("name"));
		req.setMail(request.getParameter("mail"));
		req.setMessage(request.getParameter("message"));
		try {
			requestDao.saveRequest(req);
			statusMessage = "Request saved";
		} catch (Exception e) {
			statusMessage = "Please re-enter your request";
		}

		request.setAttribute("statusMessage", statusMessage);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("contactus.jsp");
		requestDispatcher.forward(request, response);

	}

}
