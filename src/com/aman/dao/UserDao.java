package com.aman.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.aman.modelclass.User;

public class UserDao {
	public boolean checkCredential(User user) throws ClassNotFoundException, SQLException{
		Class.forName("org.postgresql.Driver");
        Connection connection= DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres","postgres","QWERTY");
        String statement = "select * from admin where username =? and password=?";
        PreparedStatement preparedStatement;
        preparedStatement = connection.prepareStatement(statement);
		preparedStatement.setString(1,  user.getUserName());
		preparedStatement.setString(2,  user.getPassword());
		ResultSet resultSet = preparedStatement.executeQuery();
		
		return resultSet.next();
		
	}

}
