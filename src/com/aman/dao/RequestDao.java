package com.aman.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.aman.modelclass.Request;

public class RequestDao {
	public void saveRequest(Request request) {
		Connection connection = null;
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "QWERTY");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		String statement = "insert into customer (name, mail, message) values(?, ?, ?)";
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection.prepareStatement(statement);
			preparedStatement.setString(1, request.getName());
			preparedStatement.setString(2, request.getMail());
			preparedStatement.setString(3, request.getMessage());
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Request> fetchRequest(boolean isactive) {
		ArrayList<Request> requestTypeData = new ArrayList<>();
		try {

			Class.forName("org.postgresql.Driver");
			Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres",
					"QWERTY");
			String statement = "select * from customer where isActive = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(statement);
			preparedStatement.setBoolean(1, isactive);//
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Request req = new Request();
				req.setId(resultSet.getInt("id"));
				req.setName(resultSet.getString("name"));
				req.setMail(resultSet.getString("mail"));
				req.setMessage(resultSet.getString("message"));
				requestTypeData.add(req);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return requestTypeData;
	}

	public void editStatus(int id) throws ClassNotFoundException, SQLException {
		Class.forName("org.postgresql.Driver");
		Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres",
				"QWERTY");
		PreparedStatement preparedStatement = connection.prepareStatement("select isactive from customer where id = ?");
		preparedStatement.setInt(1, id);
		ResultSet resultSet = preparedStatement.executeQuery();
		boolean isactive = resultSet.next();
		PreparedStatement editStatement = connection.prepareStatement("update customer set isactive = ? where id = ?");
		editStatement.setBoolean(1, !isactive);
		editStatement.setInt(2, id);
		editStatement.executeUpdate();
		connection.close();

	}

}
