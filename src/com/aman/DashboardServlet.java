package com.aman;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.aman.dao.RequestDao;
import com.aman.modelclass.Request;

@WebServlet("/dashboard")
public class DashboardServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDao requestDao = new RequestDao();
		ArrayList<Request>activeData= requestDao.fetchRequest(true);
		ArrayList<Request>archiveData= requestDao.fetchRequest(false);
		request.setAttribute("activeData",activeData);
		request.setAttribute("archiveData", archiveData);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("dashboard.jsp");
		requestDispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)	throws ServletException, IOException {
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("dashboard.jsp");
		RequestDao requestDao = new RequestDao();
		try {
			requestDao.editStatus(Integer.parseInt(request.getParameter("id")));
		} catch (NumberFormatException | ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		requestDispatcher.forward(request, response);
	}
}
